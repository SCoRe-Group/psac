#!/bin/bash

# prepare directories structure (do not edit)
mkdir -p build/
rm -rf build/*
cd build/

# Add -DCMAKE_BUILD_TYPE=Debug to debug
cmake ../ -DCMAKE_BUILD_TYPE=Release -DPSAC_BUILD_TESTS=OFF -DPSAC_ENABLE_COVERAGE=OFF

make -j8
